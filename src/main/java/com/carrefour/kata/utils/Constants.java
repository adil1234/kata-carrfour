package com.carrefour.kata.utils;

public class Constants {

    public static final String API_URL = "/api/v1";
    public static final String URL_ORDERS = "/orders";
    public static final String URL_ADDRESSES = "/addresses";
    public static final String URL_REQUESTS = "/requests";
    public static final String URL_DELIVERY = "/delivery";
}
