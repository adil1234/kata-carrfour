package com.carrefour.kata.repository;


import com.carrefour.kata.model.entity.Request;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RequestRepository extends R2dbcRepository<Request, Long> {


}
