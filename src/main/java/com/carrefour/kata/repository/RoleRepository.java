package com.carrefour.kata.repository;


import com.carrefour.kata.model.entity.Role;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface RoleRepository extends R2dbcRepository<Role, Long> {



}
