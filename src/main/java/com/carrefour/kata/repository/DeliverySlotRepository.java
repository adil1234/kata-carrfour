package com.carrefour.kata.repository;

import com.carrefour.kata.model.entity.Delivery;
import com.carrefour.kata.model.entity.DeliverySlot;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

public interface DeliverySlotRepository extends R2dbcRepository<DeliverySlot, Long> {

    Flux<DeliverySlot> findByStartTimeAfterAndAvailableIsTrueAndDelivery_Name(LocalDateTime startTime, Delivery delivery);


}
