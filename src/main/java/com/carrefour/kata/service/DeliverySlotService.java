package com.carrefour.kata.service;

import com.carrefour.kata.model.entity.Delivery;
import com.carrefour.kata.model.entity.DeliverySlot;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

public interface DeliverySlotService {

    Flux<DeliverySlot>  findDeliverySlotAvailable(LocalDateTime startTime, Delivery delivery);
}
