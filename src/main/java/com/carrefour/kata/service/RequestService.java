package com.carrefour.kata.service;


import com.carrefour.kata.model.entity.Request;
import reactor.core.publisher.Mono;

public interface RequestService {

    Mono<Request> findById(long id);

    Mono<Request>  saveOrUpdate(Request request);

}
