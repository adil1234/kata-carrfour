package com.carrefour.kata.service.impl;

import com.carrefour.kata.model.entity.Order;
import com.carrefour.kata.repository.OrderRepository;
import com.carrefour.kata.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;


    @Override
    public Mono<Order> findById(long id) {
        return orderRepository.findById(id);
    }

    @Override
    public Flux<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public Mono<Order> saveOrUpdate(Order order) {
        return orderRepository.save(order);
    }


}
