package com.carrefour.kata.service.impl;


import com.carrefour.kata.model.entity.Request;
import com.carrefour.kata.repository.RequestRepository;
import com.carrefour.kata.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

@Service
@Transactional
public class RequestServiceImpl implements RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Override
    public Mono<Request> findById(long id) {
        return requestRepository.findById(id);
    }

    @Override
    public Mono<Request>  saveOrUpdate(Request request) {
        return requestRepository.save(request);
    }

}
