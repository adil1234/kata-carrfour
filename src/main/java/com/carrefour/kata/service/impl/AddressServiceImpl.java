package com.carrefour.kata.service.impl;


import com.carrefour.kata.model.entity.Address;
import com.carrefour.kata.repository.AddressRepository;
import com.carrefour.kata.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Transactional
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;


    @Override
    public Mono<Address> findById(Long id) {
        return addressRepository.findById(id);
    }

    @Override
    public Mono<Address> saveOrUpdate(Address address) {
        return addressRepository.save(address);
    }


    @Override
    public Flux<Address> findAll() {
        return addressRepository.findAll();
    }

}
