package com.carrefour.kata.service.impl;


import com.carrefour.kata.model.entity.Delivery;
import com.carrefour.kata.model.entity.DeliverySlot;
import com.carrefour.kata.repository.DeliverySlotRepository;
import com.carrefour.kata.service.DeliverySlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

@Service
@Transactional
public class DeliverySlotServiceImpl implements DeliverySlotService {

    @Autowired
    private DeliverySlotRepository deliverySlotRepository;


    @Override
    public Flux<DeliverySlot> findDeliverySlotAvailable(LocalDateTime startTime, Delivery delivery) {
        return deliverySlotRepository.findByStartTimeAfterAndAvailableIsTrueAndDelivery_Name(startTime, delivery);
    }
}
