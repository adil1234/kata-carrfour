package com.carrefour.kata.service;


import com.carrefour.kata.model.entity.Order;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface OrderService {

    Mono<Order> findById(long id);

    Flux<Order> findAll();

    Mono<Order> saveOrUpdate(Order order);

}
