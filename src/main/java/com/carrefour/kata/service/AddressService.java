package com.carrefour.kata.service;


import com.carrefour.kata.model.entity.Address;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface AddressService {

    Mono<Address> findById(Long id);

    Flux<Address> findAll();

    Mono<Address> saveOrUpdate(Address address);

}
