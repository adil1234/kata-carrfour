package com.carrefour.kata.controller;

import com.carrefour.kata.exception.ItemNotFoundException;
import com.carrefour.kata.model.dto.OrderDto;
import com.carrefour.kata.model.entity.Order;
import com.carrefour.kata.service.OrderService;
import com.carrefour.kata.utils.Constants;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Constants.API_URL + Constants.URL_ORDERS)
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
@Tag(name = "Order Controller", description = "API pour la gestion des commandes")
public class OrderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Mono<?> getOrderById(@PathVariable("id") long id) {
        LOGGER.info("Start getOrderById with id: {}", id);
        return orderService.findById(id)
                .switchIfEmpty(Mono.error(new ItemNotFoundException())).map(
                item -> OrderDto.toDto(item).add(linkTo(methodOn(UserController.class).
                        getUserById(item.getUser().getId())).withSelfRel())
                .add(linkTo(methodOn(AddressController.class).
                        getAddressById(item.getAddressTo().getId())).withSelfRel())
                .add(linkTo(methodOn(AddressController.class).
                        getAddressById(item.getAddressFrom().getId())).withSelfRel())
        );
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Mono<?> addOrder(@RequestBody OrderDto orderDto) {
        LOGGER.info("Start addOrder: {}", orderDto);
        return orderService.saveOrUpdate(Order.toEntity(orderDto)).map(
                item -> OrderDto.toDto(item).add(linkTo(methodOn(UserController.class).
                        getUserById(item.getUser().getId())).withSelfRel())
                        .add(linkTo(methodOn(AddressController.class).
                                getAddressById(item.getAddressTo().getId())).withSelfRel())
                        .add(linkTo(methodOn(AddressController.class).
                                getAddressById(item.getAddressFrom().getId())).withSelfRel())
        );
    }


    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    @ResponseBody
    public Flux<?> getOrders() {
            return orderService.findAll().map(
                    item -> OrderDto.toDto(item).add(linkTo(methodOn(UserController.class).
                            getUserById(item.getUser().getId())).withSelfRel())
                            .add(linkTo(methodOn(AddressController.class).
                                    getAddressById(item.getAddressTo().getId())).withSelfRel())
                            .add(linkTo(methodOn(AddressController.class).
                                    getAddressById(item.getAddressFrom().getId())).withSelfRel())
            );
    }

}
