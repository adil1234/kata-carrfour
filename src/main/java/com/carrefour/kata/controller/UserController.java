package com.carrefour.kata.controller;


import com.carrefour.kata.configuration.jwt.JwtService;
import com.carrefour.kata.configuration.user.UserInfoService;
import com.carrefour.kata.configuration.request.AuthRequest;
import com.carrefour.kata.exception.ItemNotFoundException;
import com.carrefour.kata.model.dto.AddressDto;
import com.carrefour.kata.model.dto.UserDto;
import com.carrefour.kata.model.entity.User;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/auth")
@Tag(name = "User Controller", description = "API pour la gestion des utilisateur")
public class UserController {

    @Autowired
    private UserInfoService service;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/register")
    public Mono<?> addNewUser(@RequestBody UserDto userInfo) {
        return service.addUser(userInfo).map(user -> {
            if(user != null) return jwtService.generateToken(userInfo.getUsername());
            else return null;
        });
    }

    @PostMapping("/login")
    public String authenticateAndGetToken(@RequestBody AuthRequest authRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        if (authentication.isAuthenticated()) {
            return jwtService.generateToken(authRequest.getUsername());
        } else {
            throw new UsernameNotFoundException("invalid user request !");
        }
    }

    @GetMapping("/{id}")
    public Mono<UserDto> getUserById(@PathVariable("id") long id) {
        return service.getUserById(id).map(UserDto::toDto).switchIfEmpty(Mono.error(new ItemNotFoundException()));
    }

}
