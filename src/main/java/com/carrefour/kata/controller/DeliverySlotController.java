package com.carrefour.kata.controller;


import com.carrefour.kata.model.dto.DeliverySlotDto;
import com.carrefour.kata.model.entity.Delivery;
import com.carrefour.kata.service.DeliverySlotService;
import com.carrefour.kata.utils.Constants;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

@RestController
@RequestMapping(Constants.API_URL + Constants.URL_DELIVERY)
@Tag(name = "DeliverySlot Controller", description = "API pour la gestion des livraison")
public class DeliverySlotController {

    @Autowired
    private DeliverySlotService deliverySlotService;

    @RequestMapping(value = "/available", method = RequestMethod.GET)
    @ResponseBody
    public Flux<?> findDeliverySlotAvailable(@RequestParam("startdate") LocalDateTime startTime, @RequestParam("delivery") Delivery delivery) {

        return deliverySlotService.findDeliverySlotAvailable(startTime,delivery)
                .map(item -> DeliverySlotDto.toDto(item));
    }
}
