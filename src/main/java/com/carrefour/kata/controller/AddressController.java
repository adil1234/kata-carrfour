package com.carrefour.kata.controller;

import com.carrefour.kata.exception.ItemNotFoundException;
import com.carrefour.kata.model.dto.AddressDto;
import com.carrefour.kata.model.dto.UserDto;
import com.carrefour.kata.model.entity.Address;
import com.carrefour.kata.service.AddressService;
import com.carrefour.kata.utils.Constants;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Constants.API_URL + Constants.URL_ADDRESSES)
@Tag(name = "Adresse Controller", description = "API pour la gestion des adresses")
public class AddressController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddressController.class);

    @Autowired
    private  AddressService addressService;

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Mono<AddressDto> getAddressById(@PathVariable("id") long id) {
        LOGGER.info("Start getAddressById with id: {}", id);
        return addressService.findById(id)
                .switchIfEmpty(Mono.error(new ItemNotFoundException()))
                .map(item -> AddressDto.toDto(item).add(linkTo(methodOn(UserController.class).
                        getUserById(item.getUser().getId())).withSelfRel()));
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping(method = RequestMethod.GET)
    public Flux<AddressDto> getAddresses() {
        LOGGER.info("Start getAddresses ");
        Flux<Address> addresses = addressService.findAll();
        return  addresses.map(AddressDto::toDto).map(
                addressDto -> {
                    Link selfLink = linkTo(methodOn(UserController.class).
                            getUserById(addressDto.getUser().getId())).withSelfRel();
                    addressDto.add(selfLink);
                    return addressDto;
                }
        );
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @PostMapping("/save")
    public Mono<AddressDto> addNewUser(@RequestBody AddressDto addressDto) {
        return addressService.saveOrUpdate(Address.toEntity(addressDto))
                .map(item -> AddressDto.toDto(item).add(linkTo(methodOn(UserController.class).
                        getUserById(item.getUser().getId())).withSelfRel()));
    }


}
