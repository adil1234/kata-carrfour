package com.carrefour.kata.controller;

import com.carrefour.kata.exception.ItemNotFoundException;
import com.carrefour.kata.model.dto.RequestDto;
import com.carrefour.kata.model.entity.Request;
import com.carrefour.kata.service.RequestService;
import com.carrefour.kata.utils.Constants;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Constants.API_URL + Constants.URL_REQUESTS)
@Tag(name = "Request Controller", description = "API pour la gestion des requetes")
public class RequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestController.class);

    @Autowired
    private  RequestService requestService;


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Mono<?> getRequestById(@PathVariable("id") long id) {
        LOGGER.info("Start getRequestById id: {}", id);
        return requestService.findById(id)
                .switchIfEmpty(Mono.error(new ItemNotFoundException()))
                .map(item -> RequestDto.toDto(item)
                        .add(linkTo(methodOn(OrderController.class).getOrderById(item.getOrder().getId())).withSelfRel())
                        .add(linkTo(methodOn(UserController.class).getUserById(item.getCourier().getId())).withSelfRel()));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Mono<?> addRequest(@RequestBody RequestDto requestDto) {
        LOGGER.info("addRequest: {}", requestDto);
        return requestService.saveOrUpdate(Request.toEntity(requestDto)).switchIfEmpty(Mono.error(new ItemNotFoundException()))
                .map(item -> RequestDto.toDto(item)
                        .add(linkTo(methodOn(OrderController.class).getOrderById(item.getOrder().getId())).withSelfRel())
                        .add(linkTo(methodOn(UserController.class).getUserById(item.getCourier().getId())).withSelfRel()));
    }

}
