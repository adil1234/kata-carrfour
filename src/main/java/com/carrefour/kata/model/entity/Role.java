package com.carrefour.kata.model.entity;

import com.carrefour.kata.model.base.BaseEntity;
import com.carrefour.kata.model.dto.RoleDto;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Entity
@Table(name = "roles")
@Data
@Builder
public class Role extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "name")
    private String name;

    public static Role toEntity(RoleDto roleDto) {
        return Role.builder().name(roleDto.getName()).build();
    }

}
