package com.carrefour.kata.model.entity;


import com.carrefour.kata.model.base.AuditableEntity;
import com.carrefour.kata.model.dto.RequestDto;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "requests")
@Data
@Builder
public class Request extends AuditableEntity<Long>{

    private static final long serialVersionUID = 1L;

    @Column(name = "status")
    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "courier_id")
    private User courier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deliverySlot_id")
    private DeliverySlot deliverySlot;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date")
    private Date date;


    @Override
    public String toString() {
        return "Request{" +
                "order=" + order +
                ", courier=" + courier +
                '}';
    }

    public static Request toEntity(RequestDto requestDto) {
        return Request.builder().status(requestDto.getStatus()).order(Order.toEntity(requestDto.getOrder()))
                .courier(User.toEntity(requestDto.getCourier())).deliverySlot(DeliverySlot.toEntity(requestDto.getDeliveryDto()))
                .date(new Date(requestDto.getDate())).build();
    }

}
