package com.carrefour.kata.model.entity;

import com.carrefour.kata.model.base.BaseEntity;
import com.carrefour.kata.model.dto.UserDto;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Entity
@Table(name = "users")
@Data
@Builder
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "fullname")
    private String fullname;


    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private Role role;

    @Override
    public String toString() {
        return "User{" +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public static User toEntity(UserDto userDto) {
        return User.builder().email(userDto.getEmail()).username(userDto.getEmail())
                .enabled(userDto.isEnabled()).fullname(userDto.getFullname())
                .password(userDto.getPassword()).role(Role.toEntity(userDto.getRole())).build();
    }

}
