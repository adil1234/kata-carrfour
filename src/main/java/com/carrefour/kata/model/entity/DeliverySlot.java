package com.carrefour.kata.model.entity;

import com.carrefour.kata.model.base.AuditableEntity;
import com.carrefour.kata.model.dto.DeliverySlotDto;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;


@Entity
@Table(name = "DeliverySlot")
@Data
@Builder
public class DeliverySlot extends AuditableEntity<Long> {

    @Column(name = "start")
    private LocalDateTime startTime;

    @Column(name = "end")
    private LocalDateTime endTime;

    @Column(name = "available")
    private boolean available;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deliveryr_id")
    private Delivery delivery;

    public static DeliverySlot toEntity(DeliverySlotDto deliverySlotDto) {
        return  DeliverySlot.builder().startTime(deliverySlotDto.getStartTime()).endTime(deliverySlotDto.getEndTime())
                .available(deliverySlotDto.isAvailable()).delivery(Delivery.toEntity(deliverySlotDto.getDeliveryDto())).build();
    }
}
