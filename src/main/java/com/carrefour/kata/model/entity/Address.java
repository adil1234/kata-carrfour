package com.carrefour.kata.model.entity;

import com.carrefour.kata.model.base.AuditableEntity;
import com.carrefour.kata.model.dto.AddressDto;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

@Entity
@Table(name = "addresses")
@Data
@Builder
public class Address extends AuditableEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Column(name = "name")
    private String name;

    @Column(name = "postal_code")
    private int postalCode;

    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;

    @Column(name = "region")
    private String region;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonBackReference
    private User user;


    public static Address toEntity(AddressDto addressDto) {
        return  Address.builder().address(addressDto.getAddress()).city(addressDto.getCity()).country(addressDto.getCountry()).
                name(addressDto.getName()).phone(addressDto.getPhone()).postalCode(addressDto.getPostalCode()).
                region(addressDto.getRegion()).user(User.toEntity(addressDto.getUser())).build();
    }

}
