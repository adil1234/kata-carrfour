package com.carrefour.kata.model.entity;


import com.carrefour.kata.model.base.BaseEntity;
import com.carrefour.kata.model.dto.DeliveryDto;
import com.carrefour.kata.model.dto.RoleDto;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Entity
@Table(name = "delivery")
@Data
@Builder
public class Delivery extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "name")
    private String name;

    public static Delivery toEntity(DeliveryDto deliveryDto) {
        return Delivery.builder().name(deliveryDto.getName()).build();
    }

}
