package com.carrefour.kata.model.entity;


import com.carrefour.kata.model.base.AuditableEntity;
import com.carrefour.kata.model.dto.OrderDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "orders")
@Data
@Builder
public class Order extends AuditableEntity<Long> {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "delivery_price")
    private Double price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_from_id")
    private Address addressFrom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_to_id")
    private Address addressTo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Order order = (Order) o;

        if (!Objects.equals(description, order.description)) return false;
        if (!Objects.equals(price, order.price)) return false;
        if (!Objects.equals(user, order.user)) return false;
        return Objects.equals(addressFrom, order.addressFrom);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (addressFrom != null ? addressFrom.hashCode() : 0);
        return result;
    }

    public static Order toEntity(OrderDto orderDto) {
        return Order.builder().description(orderDto.getDescription()).price(orderDto.getPrice())
                .user(User.toEntity(orderDto.getCustomer())).addressFrom(Address.toEntity(orderDto.getAddressFrom()))
                .addressTo(Address.toEntity(orderDto.getAddressTo())).build();
    }

}
