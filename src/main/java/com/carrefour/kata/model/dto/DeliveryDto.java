package com.carrefour.kata.model.dto;

import com.carrefour.kata.model.entity.Delivery;
import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
@Builder
public class DeliveryDto extends RepresentationModel<DeliveryDto> {

    private String name;

    public static DeliveryDto toDto(Delivery delivery) {
        return DeliveryDto.builder().name(delivery.getName()).build();
    }
}
