package com.carrefour.kata.model.dto;

import com.carrefour.kata.model.entity.Role;
import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
@Builder
public class RoleDto extends RepresentationModel<RoleDto> {

    private String name;

    @Override
    public String toString() {
        return "RoleDto{" +
                "name='" + name + '\'' +
                '}';
    }

    public static RoleDto toDto(Role role) {
        return  RoleDto.builder().name(role.getName()).build();
    }

}
