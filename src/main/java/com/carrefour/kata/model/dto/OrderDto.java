package com.carrefour.kata.model.dto;

import com.carrefour.kata.model.entity.Order;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;


@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDto extends RepresentationModel<OrderDto> {

    private long id;
    private long date;
    private String description;
    private Double price;
    private AddressDto addressFrom;
    private AddressDto addressTo;
    private UserDto customer;

    public static OrderDto toDto(Order order) {
        return OrderDto.builder().id(order.getId()).addressFrom(AddressDto.toDto(order.getAddressFrom()))
                .addressTo(AddressDto.toDto(order.getAddressTo())).id(order.getId())
                .description(order.getDescription()).price(order.getPrice()).customer(UserDto.toDto(order.getUser())).build();
    }

}
