package com.carrefour.kata.model.dto;

import com.carrefour.kata.model.entity.User;
import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
@Builder
public class UserDto extends RepresentationModel<UserDto> {

    private long id;
    private String email;
    private String username;
    private String password;
    private boolean enabled;
    private boolean isCustomer;
    private boolean isCourier;
    private String fullname;
    private RoleDto role;

    public static UserDto toDto(User user) {
        if(user == null) return null;
        return UserDto.builder().id(user.getId()).email(user.getEmail()).username(user.getUsername())
                .password(user.getPassword()).enabled(user.isEnabled()).fullname(user.getFullname())
                .role(RoleDto.toDto(user.getRole())).build();
    }

}
