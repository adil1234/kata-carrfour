package com.carrefour.kata.model.dto;


import com.carrefour.kata.model.entity.DeliverySlot;
import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDateTime;

@Data
@Builder
public class DeliverySlotDto extends RepresentationModel<DeliverySlotDto> {

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private boolean available;

    private DeliveryDto deliveryDto;

    public static DeliverySlotDto toDto(DeliverySlot deliverySlot) {
        return DeliverySlotDto.builder().startTime(deliverySlot.getStartTime()).endTime(deliverySlot.getEndTime())
                .available(deliverySlot.isAvailable()).deliveryDto(DeliveryDto.toDto(deliverySlot.getDelivery())).build();
    }

}
