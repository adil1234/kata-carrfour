package com.carrefour.kata.model.dto;

import com.carrefour.kata.model.entity.Request;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
@Builder
public class RequestDto extends RepresentationModel<RequestDto> {

    @JsonProperty("id")
    private long id;

    @JsonProperty("status")
    private String status;

    @JsonProperty("order")
    private OrderDto order;

    @JsonProperty("courier")
    private UserDto courier;

    @JsonProperty("creation_date")
    private long date;

    @JsonProperty("delivery")
    private DeliverySlotDto deliveryDto;


    public static RequestDto toDto(Request request) {
        return  RequestDto.builder().id(request.getId()).status(request.getStatus()).order(OrderDto.toDto(request.getOrder()))
                .courier(UserDto.toDto(request.getCourier())).date( request.getCreatedDate().getTime() / 1000)
                .deliveryDto(DeliverySlotDto.toDto(request.getDeliverySlot())).build();
    }



}
