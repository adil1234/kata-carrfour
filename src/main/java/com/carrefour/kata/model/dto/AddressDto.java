package com.carrefour.kata.model.dto;


import com.carrefour.kata.model.entity.Address;
import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
@Builder
public class AddressDto extends RepresentationModel<AddressDto> {

    private long id;
    private String name;
    private int postalCode;
    private String country;
    private String city;
    private String region;
    private String address;
    private String phone;
    private UserDto user;

    public static AddressDto toDto(Address address) {
        return AddressDto.builder().address(address.getAddress()).city(address.getCity()).country(address.getCountry())
                .id(address.getId()).name(address.getPhone()).postalCode(address.getPostalCode())
                .name(address.getName()).region(address.getRegion()).user(UserDto.toDto(address.getUser())).build();
    }
}
