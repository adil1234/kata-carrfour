package com.carrefour.kata.model.enums;

public enum DeliveryEnum {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP;

    DeliveryEnum() {
    }
}
