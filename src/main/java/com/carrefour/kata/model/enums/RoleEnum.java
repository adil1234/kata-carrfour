package com.carrefour.kata.model.enums;

public enum RoleEnum {
    ROLE_ADMIN,
    ROLE_COURIER,
    ROLE_CUSTOMER;

    RoleEnum() {
    }
}
